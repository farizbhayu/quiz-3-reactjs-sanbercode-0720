import React from "react";
import { Link } from 'react-router-dom';
import '../App.css';

function Login() {
    return(
        <div className="container">
            <h1>Login Form</h1>
            <div style={{width: "50%", margin: "0 auto", display: "block"}}>
                <div style={{border: "1px solid #aaa", padding: "20px"}}>
                    <form>
                        <label style={{float: "left"}}>
                        Username:
                        </label>
                        <input 
                            style={{float: "right"}} 
                            type="text" 
                            name="username" 
                        />
                        <br/>
                        <br/>
                        <label style={{float: "left"}}>
                        Password:
                        </label>
                        <input 
                            style={{float: "right"}} 
                            type="passowrd" 
                            name="password" 
                        />
                        <br/>
                        <br/>
                        <div style={{width: "100%", paddingBottom: "20px"}}>
                            <button style={{float: "right"}}>
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}