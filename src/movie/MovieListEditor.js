import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../App.css';

const MovieListEditor = () => {
    const [daftarFilm, setDaftarFilm] = useState(null);
    const [input, setInput] = useState({
       title: "", 
       rating: "", 
       duration: "", 
       year: "", 
       genre: "", 
       description: ""
   })
    const [selectedId, setSelectedId] = useState(0);
    const [statusForm, setStatusForm] = useState("create");

    useEffect( () => {
        if (daftarFilm === null) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDaftarFilm(res.data.map(e => {
                    return {
                        id: e.id,
                        title: e.title,
                        rating: e.rating,
                        duration: e.duration,
                        year: e.year,
                        genre: e.genre,
                        description: e.description
                    }
                }))
            })
        }
    }, [daftarFilm])

    const handleChange = (event) => {
       let typeOfInput = event.target.name

      switch (typeOfInput){
         case "title":
            {
               setInput({...input, title: event.target.value});
               break;
            }
         case "rating":
            {
               setInput({...input, rating: event.target.value});
               break;
            }
         case "duration":
            {
               setInput({...input, duration: event.target.value});
               break;
            }
         case "year":
            {
               setInput({...input, year: event.target.value});
               break;
            }
         case "genre":
            {
               setInput({...input, genre: event.target.value});
               break;
            }
         case "description":
            {
               setInput({...input, description: event.target.value});
               break;
            }
         default:
            {break;} 
      }
    }

    const handleSubmit = (event) => {
       event.preventDefault();
       
       let title = input.title;
       let rating = parseInt(input.rating);
       let duration = parseInt(input.duration);
       let year = parseInt(input.year);
       let genre = input.genre;
       let description = input.description;

       if(title.replace(/\s/g, '') !== "" && genre.replace(/\s/g, '') !== ""){
         if(statusForm === "create"){
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
               title: input.title,
               rating: parseInt(input.rating),
               duration: parseInt(input.duration),
               year: parseInt(input.year),
               genre: input.genre,
               description: input.description
            })
            .then(res => {
               setDaftarFilm([
                  ...daftarFilm,
                  {
                     id: res.data.id,
                     title: input.title,
                     rating: parseInt(input.rating),
                     duration: parseInt(input.duration),
                     year: parseInt(input.year),
                     genre: genre,
                     description: description
                  }
               ])
            })
         } else if(statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {
               title: title,
               rating: rating,
               duration: duration,
               year: year,
               genre: genre,
               description: description})
            .then(() => {
               let dataFilm = daftarFilm.find(el => el.id === selectedId);
               dataFilm.title = input.title;
               dataFilm.rating = parseInt(input.rating);
               dataFilm.duration = parseInt(input.duration);
               dataFilm.year = parseInt(input.year);
               dataFilm.genre = input.genre;
               dataFilm.description = input.description;
               setDaftarFilm([...daftarFilm])
            })
         }
         setStatusForm("create")
         setSelectedId(0);
         setInput({
            title: "", 
            rating: "", 
            duration: "", 
            year: "", 
            genre: "", 
            description: ""
         })
       }
    }

    const handleEdit = (event) => {
       let idFilm = parseInt(event.target.value);
       let dataFilm = daftarFilm.find(x => x.id === idFilm);
       setInput({
          ...input,
          title: dataFilm.title,
          rating: dataFilm.rating,
          duration: dataFilm.duration,
          year: dataFilm.year,
          genre: dataFilm.genre,
          description: dataFilm.description
       });
       setSelectedId(idFilm);
       setStatusForm("edit")
    }

    const handleDelete = (event) => {
       let idFilm = parseInt(event.target.value);
       let newFilm = daftarFilm.filter(el => el.id !== idFilm)
       axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idFilm}`)
       .then(res => {
          console.log(res)
       })
       setDaftarFilm([...newFilm]);
    }


    return(
        <div className="container">
            <h1>Tabel Daftar Film</h1>
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul Film</th>
                        <th>Rating</th>
                        <th>Durasi</th>
                        <th>Year</th>
                        <th>Genre</th>
                        <th>Description</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        daftarFilm !== null && daftarFilm.map((item, index) => {
                            return(
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{item.title}</td>
                                    <td>{item.rating}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.year}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.description}</td>
                                    <td>
                                       <button onClick={handleEdit} value={item.id}>Edit</button>
                                       &nbsp;
                                       <button onClick={handleDelete} value={item.id}>
                                          Delete
                                       </button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
            {/* Form */}
            <h1>Form Film</h1>
            
            <div style={{width: "50%", margin: "0 auto", display: "block", backgroundColor: "#ddefef"}}>
               <div style={{border: "1px solid #aaa", padding: "20px"}}>
                  <form onSubmit={handleSubmit}>
                     <label style={{float: "left"}}>
                     Judul Film:
                     </label>
                     <input 
                        style={{float: "right"}} 
                        type="text" 
                        name="title" 
                        onChange={handleChange}
                        value={input.title}
                     />
                     <br/>
                     <br/>
                     <label style={{float: "left"}}>
                     Rating:
                     </label>
                     <input 
                        style={{float: "right"}} 
                        type="number" 
                        name="rating"
                        min="1"
                        max="10"
                        onChange={handleChange}
                        value={input.rating} 
                     />
                     <br/>
                     <br/>
                     <label style={{float: "left"}}>
                     Durasi:
                     </label>
                     <input 
                        style={{float: "right"}} 
                        type="number" 
                        name="duration"
                        onChange={handleChange}
                        value={input.duration}
                     />
                     <br/>
                     <br/>
                     <label style={{float: "left"}}>
                     Tahun :
                     </label>
                     <input 
                        style={{float: "right"}} 
                        type="number" 
                        name="year"
                        onChange={handleChange}
                        value={input.year}
                     />
                     <br/>
                     <br/>
                     <label style={{float: "left"}}>
                     Genre:
                     </label>
                     <input 
                        style={{float: "right"}} 
                        type="text" 
                        name="genre" 
                        onChange={handleChange}
                        value={input.genre}
                     />
                     <br/>
                     <br/>
                     <label style={{float: "left"}}>
                        Deskripsi:
                     </label>
                     <textarea 
                        style={{float: "right"}}
                        name="description"
                        onChange={handleChange} 
                        value={input.description}></textarea>
                     <br/>
                     <br/>
                     <br />
                     <div style={{width: "100%", paddingBottom: "20px"}}>
                     <button style={{ float: "right"}}>Submit</button>
                     </div>
                  </form>
               </div>
            </div>
        </div>
    );
}

export default MovieListEditor;