import React from "react";
import { Link } from "react-router-dom";
import "./css/Nav.css";
import logo from "./img/logo.png";

const Navbar = () => {
    return(
        <header>
            <img src={logo} alt="logo"/>
            <nav>
                <ul>
                    <li>
                        <Link to="/" >Home</Link>
                    </li>
                    <li>
                        <Link to="/about" >About</Link>
                    </li>
                    <li>
                        <Link to="/mle" >Movie List Editor</Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Navbar;