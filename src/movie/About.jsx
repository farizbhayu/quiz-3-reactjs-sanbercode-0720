import React from "react";
import '../App.css';

const About = () => {
    return (
        <div className="container">
            <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
                <li><strong style={{width: "100px"}}>Nama:</strong> Fariz Bhayu Aji</li>
                <li><strong style={{width: "100px"}}>Email:</strong> farizba56@gmail.com</li>
                <li><strong style={{width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                <li><strong style={{width: "100px"}}>Akun Gitlab:</strong> farizbhayu</li>
                <li><strong style={{width: "100px"}}>Akun Telegram:</strong> farizbhayu</li>
            </ol>
        </div>
    );
}

export default About;