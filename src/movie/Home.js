import React, { Component } from "react";
import axios from "axios";
import "../App.css";

export default class Home extends Component{
    state = {
        film: []
    }

    componentDidMount(){
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
            const film = res.data;
            this.setState({ film });
        })
    }

    render(){
        return(
            <div className="container">
                <h1>Daftar Film-Film Terbaik</h1>
                <div className="article-list">
                    {
                    this.state.film.sort((a, b) => a.rating < b.rating ? 10 : -1)
                    .map(film => 
                        <div style={{borderBottom: "1px dashed grey", marginBottom: "0"}}>
                        <h2>{film.title}</h2>
                        <br />
                        <p><strong>Rating {film.rating}</strong></p>
                        <p><strong>Durasi: {film.duration} menit</strong></p>
                    <p><strong>Genre: {film.genre}</strong></p>
                    <p style={{marginTop: "1rem"}}><strong>Deskripsi: </strong> {film.description}</p>
                    <br />
                    </div>
                    )
                    }
                </div>
            </div>
        )
    }

}

// const Home = () => {
//     return (
//         <>
//         <div className="container">
//             <h1>Daftar Film-Film Terbaik</h1>
//             <div className="article-list">
//                 <h5>Judul Film</h5>
//                 <br />
//                 <p><strong>Rating </strong>Nilai</p>
//                 <p><strong>Durasi: </strong>waktunya</p>
//                 <p><strong>Genre: </strong>genrenya</p>
//                 <br />
//                 <p><strong>Deskripsi: </strong> deskripsinya</p>
//             </div>
//         </div>
//             <footer>
//             <h5>copyright &copy; 2020 by Sanbercode</h5>
//         </footer>
//         </>
//     );
// };