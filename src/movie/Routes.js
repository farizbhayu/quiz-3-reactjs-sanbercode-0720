import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './Home';
import About from './About';
import Mle from './MovieListEditor';
import Footer from './Footer';
import Navbar from './Navbar';

export default function App() {
    return(
        <>
        <Navbar />
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route path="/about">
                <About />
            </Route>
            <Route path="/mle">
                <Mle />
            </Route>
        </Switch>
        <Footer />
        </>
    );
}